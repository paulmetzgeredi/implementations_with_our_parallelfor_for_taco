# README #

This repository is for the TACO reviewers of our paper "Device-Hopping: Transparent Mid-Kernel Runtime Switching for Heterogeneous Systems".
The repository contains implementations of the benchmarks with our parallel_for and its access pattern attributes.

The source code files contain in some cases a significant amount of code from the original implementations.
This code, for example, generates inputs or implements functions that are called by the kernels.
Therefore, we provide the line numbers at which the parallel_for instances can be found for each source code file below.

|File|Lines|
|:--|:--|
|rodinia_btree_find_k.cpp       | 1153 to 1196|
|rodinia_btree_find_range.cpp:  | 1196 to 1257|
|rodinia_nn.cpp:                | 133  to  149|
|shoc_fft.cpp:                  | 374  to  401|
|shoc_gemm_nn.cpp:              | 102  to  174|
|shoc_ifft.cpp:                 | 381  to  413|
|shoc_md5hash.cpp:              | 280  to  321|
|shoc_reduction.cpp:            |  47  to   55|
|shoc_spmv_scalar.cpp:          | 159  to  191|